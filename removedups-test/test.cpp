#include "pch.h"

#include <cstring>

#include "../removedups/removedups.h"

#define BUFFER_MAX_SIZE 256

bool TestConstStr(const char *str, const char *expected) {
    static char buffer[BUFFER_MAX_SIZE];
    strncpy_s(buffer, str, sizeof(buffer));
    RemoveDups(buffer);

    return strncmp(buffer, expected, BUFFER_MAX_SIZE) == 0;
}

TEST(case1, empty) {
    EXPECT_TRUE(TestConstStr("", ""));
}

TEST(case1, spaces1) {
    EXPECT_TRUE(TestConstStr("AAA    BBB CCC", "A B C"));
}

TEST(case1, test1) {
    EXPECT_TRUE(TestConstStr("AAA BBB CCC", "A B C"));
}

TEST(case1, test2) {
    EXPECT_TRUE(TestConstStr("AAAaaAAA   BBB CCC", "AaA B C"));
}

TEST(case1, test3) {
    EXPECT_TRUE(TestConstStr("ASDASDDDDaaaa  GG ggggg TTtt TT", "ASDASDa G g Tt T"));
}
