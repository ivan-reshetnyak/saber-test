﻿Задание:
> Реализуйте функции сериализации и десериализации двусвязного списка в бинарном формате в
> файл. Алгоритмическая сложность решения должна быть меньше квадратичной.

Ответ - функции ```void List::Serialize(FILE *file) const;``` и ```void List::Deserialize(FILE *file);``` в файле ```list.cpp```, обе сложности nlogn
