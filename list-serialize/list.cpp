#include <map>
#include <vector>

#include "list.h"

List::List() {
    head = tail = nullptr;
    count = 0;
}

List::~List() {
    Clear();
}

bool List::IsEmpty() const {
    return count <= 0;
}

void List::Clear() {
    while (head) {
        auto old = head;
        head = head->next;
        delete old;
    }
    head = tail = nullptr;
    count = 0;
}

void List::PushBack(ListNode *node) {
    if (!head) {
        // Empty list
        head = tail = node;
        count = 1;
        node->prev = node->prev = nullptr;
        return;
    }

    tail->next = node;
    node->prev = tail;
    node->next = nullptr;
    tail = node;
    ++count;
}

ListNode * List::PushBack(const std::string &data, ListNode *rand) {
    auto newNode = new ListNode{nullptr, nullptr, rand, data};
    PushBack(newNode);
    return newNode;
}

void List::Serialize(FILE *file) const {
    if (IsEmpty()) {
        return;
    }

    // Can be cached if we don't care about memory
    std::map<ListNode *, int> nummap;
    auto ptr = head;
    for (int i = 0; i < count; ++i) {
        nummap.insert({ptr, i});
        ptr = ptr->next;
    }

    ptr = head;
    while (ptr) {
        int ref = -1;
        if (ptr->rand) {
            auto it = nummap.find(ptr->rand);
            if (it != nummap.end()) {
                // Only write valid references
                ref = it->second;
            }
        }
        fwrite(&ref, sizeof(ref), 1, file);

        size_t len = ptr->data.length();
        fwrite(&len, sizeof(len), 1, file);
        fwrite(ptr->data.c_str(), sizeof(char), len, file);

        ptr = ptr->next;
    }
}

void List::Deserialize(FILE *file) {
    Clear();

    // Can be cached if we don't care about memory
    std::vector<ListNode *> ind2ptr;
    std::map<ListNode *, int> requests;
    while (true) {
        int ref;
        size_t len;
        char *buf;

        if (fread(&ref, sizeof(ref), 1, file) != 1) {
            // No more entries
            break;
        }

        if (fread(&len, sizeof(len), 1, file) != 1) {
            // Invalid format
            break;
        }

        buf = new char[len + 1]{0};
        if (fread(buf, sizeof(char), len, file) != len) {
            // Invalid format
            delete[] buf;
            break;
        }
        std::string str = buf;
        delete[] buf;

        // Valid node can be created
        auto newNode = PushBack(str);
        ind2ptr.push_back(newNode);
        if (ref != -1) {
            requests.insert({newNode, ref});
        }
    }

    for (const auto &it : requests) {
        if (it.second < ind2ptr.size()) {
            // Valid reference, link it
            it.first->rand = ind2ptr[it.second];
        }
    }
}

bool List::operator==(const List &other) const {
    if (count != other.count) {
        // Sizes are different
        return false;
    }

    // Also can be cached
    std::map<ListNode *, int> map1, map2;

    int cnt = 0;
    ListNode *ptr1 = head, *ptr2;
    while (ptr1) {
        map1.insert({ptr1, cnt});
        ptr1 = ptr1->next;
        ++cnt;
    }

    cnt = 0;
    ptr1 = other.head;
    while (ptr1) {
        map2.insert({ptr1, cnt});
        ptr1 = ptr1->next;
        ++cnt;
    }

    ptr1 = head;
    ptr2 = other.head;
    while (ptr1 && ptr2) {
        if (ptr1->data != ptr2->data) {
            // Data mismatch
            return false;
        }

        if (ptr1->rand != nullptr && ptr2->rand != nullptr) {
            // Both are linked
            auto it1 = map1.find(ptr1->rand), it2 = map2.find(ptr2->rand);
            if (it1 == map1.end() || it2 == map2.end()) {
                // At least one reference is invalid
                return false;
            }
            if (it1->second != it2->second) {
                // Relative linking is different
                return false;
            }
        } else {
            if ((ptr1->rand == nullptr || ptr2->rand == nullptr) && ptr1->rand != ptr2->rand) {
                // Only one is not linked
                return false;
            }
        }

        ptr1 = ptr1->next;
        ptr2 = ptr2->next;
    }

    return true;
}
