#pragma once

#include <cstdio>
#include <string>

// CANNOT BE MODIFIED; TO BE USED AS-IS
struct ListNode {
    ListNode *prev;
    ListNode *next;
    ListNode *rand; // Pointer to a random list element, may be NULL
    std::string data;
};

class List {
public:
    List();
    ~List();

    bool IsEmpty() const;
    void Clear();

    /// <summary>
    /// Write to file, O(nlogn) complexity.
    /// Invalid ListNode->rand pointers will be DISCARDED.
    /// </summary>
    /// <param name="file">File pointer opened with fopen(path, "wb")</param>
    void Serialize(FILE *file) const;
    // Read from file (opened with fopen(path, "rb")), O(nlogn) complexity.
    void Deserialize(FILE *file);

    /// <summary>
    /// Takes ownership.
    /// Does not modify node->rand nor check its validity.
    /// </summary>
    void PushBack(ListNode *node);

    /// <summary>
    /// Creates new ListNode, assumes ownership.
    /// </summary>
    /// <returns>Pointer to constructed ListNode</returns>
    ListNode * PushBack(const std::string &data, ListNode *rand = nullptr);

    /// <summary>
    /// Compare structure (data and relative links).
    /// If any link is invalid will return false.
    /// </summary>
    bool operator==(const List &) const;

private:
    ListNode *head;
    ListNode *tail;
    int count;
};
