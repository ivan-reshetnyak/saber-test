#include <iostream>
#include <bitset>

void printbin(int x) {
    unsigned xu = *(unsigned *)&x;
    int digit = sizeof(int) * 8 - 1;
    while (digit >= 0) {
        std::cout << ((xu >> digit--) & 1);
    }
}

void printcase(int x) {
    printbin(x);
    std::cout << "- from " << x << std::endl
              << std::bitset<sizeof(int) * 8>(x) << " - expected" << std::endl << std::endl;
}

int main(int argc, char *argv[]) {
    printcase(0);
    printcase(1);
    printcase(2);
    printcase(4);
    printcase(8);
    printcase(1024);

    printcase(-1);
    printcase(-2);
    printcase(-4);
    printcase(-8);

    printcase(123456);
    printcase(-2135234);
    printcase(1 << (sizeof(int) * 8 - 1));
}
