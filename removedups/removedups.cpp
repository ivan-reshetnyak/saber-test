void RemoveDups(char *str) {
    if (*str == 0) {
        return;
    }

    char *anchor = str, *fwd = str + 1;

    while (*fwd) {
        if (*fwd != *anchor) {
            *(++anchor) = *fwd;
        }
        ++fwd;
    }
    *(anchor + 1) = 0;
}
