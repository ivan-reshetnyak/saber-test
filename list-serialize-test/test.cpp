#include "pch.h"

#include <cstdio>

#include <vector>

#include "../list-serialize/list.h"

#define SAVEFILE_NAME "ser.temp"

TEST(Comparison, true1) {
    List list1, list2;

    std::vector<ListNode *>
        nodes1 = { new ListNode{nullptr, nullptr, nullptr, "0"}, new ListNode{nullptr, nullptr, nullptr, "1"} },
        nodes2 = { new ListNode{nullptr, nullptr, nullptr, "0"}, new ListNode{nullptr, nullptr, nullptr, "1"} };

    for (const auto &it : nodes1) {
        list1.PushBack(it);
    }
    for (const auto &it : nodes2) {
        list2.PushBack(it);
    }

    ASSERT_TRUE(list1 == list2);
}

TEST(Comparison, true2) {
    List list1, list2;

    std::vector<ListNode *>
        nodes1 = { new ListNode{nullptr, nullptr, nullptr, "0"}, new ListNode{nullptr, nullptr, nullptr, "1"},
                   new ListNode{nullptr, nullptr, nullptr, "2"} },
        nodes2 = { new ListNode{nullptr, nullptr, nullptr, "0"}, new ListNode{nullptr, nullptr, nullptr, "1"},
                   new ListNode{nullptr, nullptr, nullptr, "2"} };
    nodes1[1]->rand = nodes1[0];
    nodes2[1]->rand = nodes2[0];

    for (const auto &it : nodes1) {
        list1.PushBack(it);
    }
    for (const auto &it : nodes2) {
        list2.PushBack(it);
    }

    ASSERT_TRUE(list1 == list2);
}

TEST(Comparison, false1) {
    List list1, list2;

    std::vector<ListNode *>
        nodes1 = { new ListNode{nullptr, nullptr, nullptr, "0"}, new ListNode{nullptr, nullptr, nullptr, "1"} },
        nodes2 = { new ListNode{nullptr, nullptr, nullptr, "0"}, new ListNode{nullptr, nullptr, nullptr, "2"} };

    for (const auto &it : nodes1) {
        list1.PushBack(it);
    }
    for (const auto &it : nodes2) {
        list2.PushBack(it);
    }

    ASSERT_FALSE(list1 == list2);
}

TEST(Comparison, false2) {
    List list1, list2;

    std::vector<ListNode *>
        nodes1 = { new ListNode{nullptr, nullptr, nullptr, "0"}, new ListNode{nullptr, nullptr, nullptr, "1"},
                   new ListNode{nullptr, nullptr, nullptr, "2"} },
        nodes2 = { new ListNode{nullptr, nullptr, nullptr, "0"}, new ListNode{nullptr, nullptr, nullptr, "1"},
                   new ListNode{nullptr, nullptr, nullptr, "2"} };
    nodes1[1]->rand = nodes1[0];
    nodes2[1]->rand = nodes2[2];

    for (const auto &it : nodes1) {
        list1.PushBack(it);
    }
    for (const auto &it : nodes2) {
        list2.PushBack(it);
    }

    ASSERT_FALSE(list1 == list2);
}

TEST(SerializeDeserialize, simple1) {
    List list1, list2;
    list1.PushBack("data");

    FILE *file = fopen(SAVEFILE_NAME, "wb");
    list1.Serialize(file);
    fclose(file);

    file = fopen(SAVEFILE_NAME, "rb");
    list2.Deserialize(file);
    fclose(file);

    ASSERT_TRUE(list1 == list2);
}

TEST(SerializeDeserialize, links1) {
    List list1, list2;

    std::vector<ListNode *> nodes = {
        new ListNode{nullptr, nullptr, nullptr, "0"}, new ListNode{nullptr, nullptr, nullptr, "1"},
        new ListNode{nullptr, nullptr, nullptr, "2"}, new ListNode{nullptr, nullptr, nullptr, "3"},
        new ListNode{nullptr, nullptr, nullptr, "4"}, new ListNode{nullptr, nullptr, nullptr, "5"}
    };
    nodes[1]->rand = nodes[0];
    nodes[2]->rand = nodes[2];
    nodes[3]->rand = nodes[1];
    nodes[4]->rand = nodes[5];

    for (const auto &it : nodes) {
        list1.PushBack(it);
    }

    FILE *file = fopen(SAVEFILE_NAME, "wb");
    list1.Serialize(file);
    fclose(file);

    file = fopen(SAVEFILE_NAME, "rb");
    list2.Deserialize(file);
    fclose(file);

    ASSERT_TRUE(list1 == list2);
}
